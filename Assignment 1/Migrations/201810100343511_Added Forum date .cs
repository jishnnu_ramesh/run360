namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedForumdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fora", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fora", "Date");
        }
    }
}
