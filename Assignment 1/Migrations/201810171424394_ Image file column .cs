namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Imagefilecolumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ForumMessages", "Image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ForumMessages", "Image");
        }
    }
}
