namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BookingTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bookings",
                c => new
                    {
                        BookingId = c.Int(nullable: false, identity: true),
                        RaceType = c.String(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                        Event_EventId = c.Int(),
                    })
                .PrimaryKey(t => t.BookingId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .ForeignKey("dbo.Events", t => t.Event_EventId)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Event_EventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bookings", "Event_EventId", "dbo.Events");
            DropForeignKey("dbo.Bookings", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Bookings", new[] { "Event_EventId" });
            DropIndex("dbo.Bookings", new[] { "ApplicationUser_Id" });
            DropTable("dbo.Bookings");
        }
    }
}
