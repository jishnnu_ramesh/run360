namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changestoorganizerprofile : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.OrgProfiles", newName: "OrganizerProfiles");
            AlterColumn("dbo.OrganizerProfiles", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.OrganizerProfiles", "LastName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.OrganizerProfiles", "LastName", c => c.String());
            AlterColumn("dbo.OrganizerProfiles", "FirstName", c => c.String());
            RenameTable(name: "dbo.OrganizerProfiles", newName: "OrgProfiles");
        }
    }
}
