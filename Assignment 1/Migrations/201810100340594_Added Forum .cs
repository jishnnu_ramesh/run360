namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedForum : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Fora",
                c => new
                    {
                        ForumId = c.Int(nullable: false, identity: true),
                        WelcomeMessage = c.String(nullable: false),
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                        Event_EventId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ForumId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id, cascadeDelete: true)
                .ForeignKey("dbo.Events", t => t.Event_EventId, cascadeDelete: true)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Event_EventId);
            
            CreateTable(
                "dbo.ForumMessages",
                c => new
                    {
                        ForumMessageId = c.Int(nullable: false, identity: true),
                        Message = c.String(nullable: false),
                        Votes = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                        Event_EventId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ForumMessageId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id, cascadeDelete: true)
                .ForeignKey("dbo.Events", t => t.Event_EventId, cascadeDelete: true)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Event_EventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ForumMessages", "Event_EventId", "dbo.Events");
            DropForeignKey("dbo.ForumMessages", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Fora", "Event_EventId", "dbo.Events");
            DropForeignKey("dbo.Fora", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ForumMessages", new[] { "Event_EventId" });
            DropIndex("dbo.ForumMessages", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Fora", new[] { "Event_EventId" });
            DropIndex("dbo.Fora", new[] { "ApplicationUser_Id" });
            DropTable("dbo.ForumMessages");
            DropTable("dbo.Fora");
        }
    }
}
