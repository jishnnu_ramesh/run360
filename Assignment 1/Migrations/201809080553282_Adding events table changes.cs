namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingeventstablechanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventModels", "EventType", c => c.String());
            DropColumn("dbo.EventModels", "EventType_DataGroupField");
            DropColumn("dbo.EventModels", "EventType_DataTextField");
            DropColumn("dbo.EventModels", "EventType_DataValueField");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EventModels", "EventType_DataValueField", c => c.String());
            AddColumn("dbo.EventModels", "EventType_DataTextField", c => c.String());
            AddColumn("dbo.EventModels", "EventType_DataGroupField", c => c.String());
            DropColumn("dbo.EventModels", "EventType");
        }
    }
}
