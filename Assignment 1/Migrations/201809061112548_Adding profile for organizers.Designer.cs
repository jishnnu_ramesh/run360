// <auto-generated />
namespace Assignment_1.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Addingprofilefororganizers : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Addingprofilefororganizers));
        
        string IMigrationMetadata.Id
        {
            get { return "201809061112548_Adding profile for organizers"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
