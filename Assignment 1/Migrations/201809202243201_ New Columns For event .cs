namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewColumnsForevent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "City", c => c.String());
            AddColumn("dbo.Events", "State", c => c.String());
            AddColumn("dbo.Events", "Country", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "Country");
            DropColumn("dbo.Events", "State");
            DropColumn("dbo.Events", "City");
        }
    }
}
