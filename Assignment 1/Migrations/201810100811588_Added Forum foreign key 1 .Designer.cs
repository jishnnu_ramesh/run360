// <auto-generated />
namespace Assignment_1.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddedForumforeignkey1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedForumforeignkey1));
        
        string IMigrationMetadata.Id
        {
            get { return "201810100811588_Added Forum foreign key 1 "; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
