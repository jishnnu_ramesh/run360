namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventTypetablebooleanscorrected : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Events", "FullMarathon");
            DropColumn("dbo.Events", "HalfMarathon");
            DropColumn("dbo.Events", "TenkmRace");
            DropColumn("dbo.Events", "FivekmRace");
            AddColumn("dbo.Events", "FivekmRace", c => c.Boolean(nullable: false));
            AddColumn("dbo.Events", "TenkmRace", c => c.Boolean(nullable: false));
            AddColumn("dbo.Events", "HalfMarathon", c => c.Boolean(nullable: false));
            AddColumn("dbo.Events", "FullMarathon", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "FullMarathon");
            DropColumn("dbo.Events", "HalfMarathon");
            DropColumn("dbo.Events", "TenkmRace");
            DropColumn("dbo.Events", "FivekmRace");
        }
    }
}
