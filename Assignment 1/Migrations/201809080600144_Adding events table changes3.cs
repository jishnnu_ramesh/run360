namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingeventstablechanges3 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.EventModels");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EventModels",
                c => new
                    {
                        EventId = c.Int(nullable: false, identity: true),
                        EventName = c.String(nullable: false),
                        Location = c.String(nullable: false),
                        DateOfEvent = c.DateTime(nullable: false),
                        EventType = c.String(),
                    })
                .PrimaryKey(t => t.EventId);
            
        }
    }
}
