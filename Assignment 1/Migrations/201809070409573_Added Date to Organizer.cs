namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDatetoOrganizer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrganizerProfiles", "DateOfBirth", c => c.DateTime(nullable: false));
            DropColumn("dbo.OrganizerProfiles", "Age");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrganizerProfiles", "Age", c => c.Int(nullable: false));
            DropColumn("dbo.OrganizerProfiles", "DateOfBirth");
        }
    }
}
