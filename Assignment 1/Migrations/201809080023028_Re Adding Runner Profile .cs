namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReAddingRunnerProfile : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RunnerProfiles",
                c => new
                    {
                        ProfileId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        DateOfBirth = c.DateTime(nullable: false),
                        Height = c.Double(nullable: false),
                        weight = c.Double(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ProfileId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RunnerProfiles", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.RunnerProfiles", new[] { "ApplicationUser_Id" });
            DropTable("dbo.RunnerProfiles");
        }
    }
}
