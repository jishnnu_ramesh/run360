namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingeventstablechanges5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "organizer_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Events", "organizer_Id");
            AddForeignKey("dbo.Events", "organizer_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Events", "organizer_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Events", new[] { "organizer_Id" });
            DropColumn("dbo.Events", "organizer_Id");
        }
    }
}
