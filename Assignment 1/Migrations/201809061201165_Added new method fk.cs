namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addednewmethodfk : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrgProfiles", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.OrgProfiles", "ApplicationUser_Id");
            AddForeignKey("dbo.OrgProfiles", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrgProfiles", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.OrgProfiles", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.OrgProfiles", "ApplicationUser_Id");
        }
    }
}
