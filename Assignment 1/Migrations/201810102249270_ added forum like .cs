namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedforumlike : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ForumLikes",
                c => new
                    {
                        ForumLikeId = c.Int(nullable: false, identity: true),
                        Votes = c.Int(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                        ForumMessages_ForumMessageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ForumLikeId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .ForeignKey("dbo.ForumMessages", t => t.ForumMessages_ForumMessageId, cascadeDelete: true)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.ForumMessages_ForumMessageId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ForumLikes", "ForumMessages_ForumMessageId", "dbo.ForumMessages");
            DropForeignKey("dbo.ForumLikes", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ForumLikes", new[] { "ForumMessages_ForumMessageId" });
            DropIndex("dbo.ForumLikes", new[] { "ApplicationUser_Id" });
            DropTable("dbo.ForumLikes");
        }
    }
}
