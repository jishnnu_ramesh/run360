namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingprofilefororganizers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrgProfiles",
                c => new
                    {
                        ProfileId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Age = c.Int(nullable: false),
                        ApplicationUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ProfileId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .Index(t => t.ApplicationUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrgProfiles", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.OrgProfiles", new[] { "ApplicationUserId" });
            DropTable("dbo.OrgProfiles");
        }
    }
}
