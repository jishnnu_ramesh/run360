namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedForumforeignkey3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ForumMessages", "Event_EventId", c => c.Int(nullable: false));
            CreateIndex("dbo.ForumMessages", "Event_EventId");
            AddForeignKey("dbo.ForumMessages", "Event_EventId", "dbo.Events", "EventId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ForumMessages", "Event_EventId", "dbo.Events");
            DropIndex("dbo.ForumMessages", new[] { "Event_EventId" });
            DropColumn("dbo.ForumMessages", "Event_EventId");
        }
    }
}
