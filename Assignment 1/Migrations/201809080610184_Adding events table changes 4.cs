namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingeventstablechanges4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                {
                    EventId = c.Int(nullable: false, identity: true),
                    EventName = c.String(nullable: false),
                    Location = c.String(nullable: false),
                    DateOfEvent = c.DateTime(nullable: false),
                    FiveKmRace = c.Boolean(nullable: false),
                    TenKmRace = c.Boolean(nullable: false),
                    HalfMarathon = c.Boolean(nullable: false),
                    FullMarathon = c.Boolean(nullable: false),
                    EventType = c.String(nullable:false)
                 
                    })
                .PrimaryKey(t => t.EventId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Events");
        }
    }
}
