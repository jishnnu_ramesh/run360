namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cascadedelete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Bookings", "Event_EventId", "dbo.Events");
            DropIndex("dbo.Bookings", new[] { "Event_EventId" });
            AlterColumn("dbo.Bookings", "Event_EventId", c => c.Int(nullable: false));
            CreateIndex("dbo.Bookings", "Event_EventId");
            AddForeignKey("dbo.Bookings", "Event_EventId", "dbo.Events", "EventId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bookings", "Event_EventId", "dbo.Events");
            DropIndex("dbo.Bookings", new[] { "Event_EventId" });
            AlterColumn("dbo.Bookings", "Event_EventId", c => c.Int());
            CreateIndex("dbo.Bookings", "Event_EventId");
            AddForeignKey("dbo.Bookings", "Event_EventId", "dbo.Events", "EventId");
        }
    }
}
