namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventTypetablechangedatatype2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Events", "EventType", c => c.String(nullable: false, maxLength: 8000, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Events", "EventType", c => c.String(maxLength: 8000, unicode: false));
        }
    }
}
