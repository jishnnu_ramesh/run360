namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedfK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrgProfiles", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.OrgProfiles", new[] { "ApplicationUserId" });
            DropColumn("dbo.OrgProfiles", "ApplicationUserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrgProfiles", "ApplicationUserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.OrgProfiles", "ApplicationUserId");
            AddForeignKey("dbo.OrgProfiles", "ApplicationUserId", "dbo.AspNetUsers", "Id");
        }
    }
}
