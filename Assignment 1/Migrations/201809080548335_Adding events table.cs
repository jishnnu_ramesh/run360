namespace Assignment_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingeventstable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EventModels",
                c => new
                    {
                        EventId = c.Int(nullable: false, identity: true),
                        EventName = c.String(nullable: false),
                        Location = c.String(nullable: false),
                        DateOfEvent = c.DateTime(nullable: false),
                        EventType_DataGroupField = c.String(),
                        EventType_DataTextField = c.String(),
                        EventType_DataValueField = c.String(),
                    })
                .PrimaryKey(t => t.EventId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EventModels");
        }
    }
}
