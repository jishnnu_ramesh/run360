﻿using Assignment_1.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Assignment_1.Startup))]
namespace Assignment_1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRoles();
        }

        private void CreateRoles() {

            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (!roleManager.RoleExists("Admin")) {

                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Runner")) {

                var role = new IdentityRole();
                role.Name = "Runner";
                roleManager.Create(role);

            }

            if (!roleManager.RoleExists("Organizer")) {

                var role = new IdentityRole();
                role.Name = "Organizer";
                roleManager.Create(role);

            }

       
        }
    }
}
