﻿



function showRunnerRegister() {
    document.querySelector("#user-type").style.display = "none";
    document.querySelector("#register-head").style.display = "none";
    document.querySelector("#register-form").style.display = "block"; 
    document.querySelector('.user-role-text').value = "Runner";
}


function showOrganizerRegister() {
    document.querySelector("#user-type").style.display = "none";
    document.querySelector("#register-head").style.display = "none";
    document.querySelector("#register-form").style.display = "block";
    document.querySelector('.user-role-text').value = "Organizer";

}


function showOptions() {

    document.querySelector("#user-type").style.display = "block";
    document.querySelector("#register-head").style.display = "block";
    document.querySelector("#register-form").style.display = "none";
    document.querySelector('.user-role-text').value = "";

}



function runnerHover() {

   // document.querySelector("#runner-text").style.color = "black"

}


document.querySelector(".runner").onclick = showRunnerRegister;
document.querySelector(".organizer").onclick = showOrganizerRegister;
document.querySelector(".go-back-button").onclick = showOptions;
