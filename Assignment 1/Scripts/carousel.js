﻿// for all sliders
let sliders;

//sets all sliders to display none
function reset() {

   
    for (let i = 1; i < sliders.length; i++) {

        sliders[i].style.display = "none";
        console.log(sliders[i]);
    }

}

function animation() {

    let index = 0;


    setInterval(function () {


        sliders[index].style.display = "none";
        index = (index + 1) % 3;
        sliders[index].style.display = "block";


    },10000);


}



//init function
function startSlide() {
    sliders = document.querySelectorAll('.slide');
    reset();
    animation();
}

document.onload = startSlide();