﻿using Assignment_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        public ActionResult Index()
        {
            HomeModel homeModel = new HomeModel();


            IEnumerable<Event> upcomingEvents = db.Event.Where(m => m.DateOfEvent > DateTime.Now).OrderBy(m => m.DateOfEvent).Take(4).ToList();

            SearchViewModel searchViewModel = new SearchViewModel();
 
            homeModel.Event = upcomingEvents;

            homeModel.SearchViewModel = searchViewModel;

            homeModel.SearchResult = null;

            homeModel.ResultAvailable = false;
            

            return View(homeModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "SearchText,StartDate,EndDate")] SearchViewModel searchViewModel)
        {

            HomeModel homeModel = new HomeModel();

            // Repopulate the upcoming events
            IEnumerable<Event> upcomingEvents = db.Event.Where(m => m.DateOfEvent > DateTime.Now).OrderBy(m => m.DateOfEvent).Take(4).ToList();

            homeModel.Event = upcomingEvents;

            // Bind the model back
            homeModel.SearchViewModel = searchViewModel;

            //set for storing the search results
            HashSet<Event> eventSet = new HashSet<Event>();

            homeModel.SearchResult = eventSet;

            homeModel.ResultAvailable = false;


       



            if (ModelState.IsValid)
            {

    
                IEnumerable<Event> searchResultsEventName = db.Event.Where(m => m.DateOfEvent >= searchViewModel.StartDate && 
                m.EventName.Contains(searchViewModel.SearchText) &&
                m.DateOfEvent <= searchViewModel.EndDate).ToList();

                IEnumerable<Event> searchResultsLocation = db.Event.Where(m => m.DateOfEvent >= searchViewModel.StartDate &&
                m.Location.Contains(searchViewModel.SearchText) &&
                m.DateOfEvent <= searchViewModel.EndDate).ToList();

                IEnumerable<Event> searchResultsCity = db.Event.Where(m => m.DateOfEvent >= searchViewModel.StartDate &&
                m.City.Contains(searchViewModel.SearchText) &&
                m.DateOfEvent <= searchViewModel.EndDate).ToList();

                foreach (var item in searchResultsEventName)
                {
                    try
                    {
                        eventSet.Add(item);
                    }
                    catch
                    {
                        Console.WriteLine("Test");
                    }

                }

                foreach (var item in searchResultsLocation)
                {
                    try
                    {
                        eventSet.Add(item);
                    }
                    catch
                    {
                        Console.WriteLine("Test");
                    }

                }


                foreach (var item in searchResultsCity)
                {
                    try
                    {
                        eventSet.Add(item);
                    }
                    catch
                    {
                        Console.WriteLine("Test");
                    }

                }




                homeModel.SearchResult = eventSet;

                homeModel.ResultAvailable = true;

                return View(homeModel);
            }


            return View(homeModel);



        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

       
    }
}