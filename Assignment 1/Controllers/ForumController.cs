﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment_1.Models;
using Microsoft.AspNet.Identity;

namespace Assignment_1.Controllers
{
    public class ForumController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Forum
        [Authorize(Roles ="Organizer")]
        public ActionResult ManageForum()
        {
            ApplicationUser user = db.Users.Find(User.Identity.GetUserId());

            return View(db.Forum.Where(f => f.ApplicationUser.Id == user.Id).ToList());
        }


        [Authorize(Roles = "Runner")]
        public ActionResult RunnerIndex()
        {
            ApplicationUser user = db.Users.Find(User.Identity.GetUserId());
            IEnumerable<Bookings> bookings = db.Bookings.Where(o => o.ApplicationUser.Id == user.Id  && o.Status == true ).ToList();

            ArrayList eventId = new ArrayList();

            ArrayList Forums = new ArrayList();

            foreach (Bookings b in bookings)
            {
                eventId.Add(b.Event.EventId);

            }

            foreach (int id in eventId)
            {
                
                Forum forum = db.Forum.Where(f => f.Event.EventId == id).First();
                Forums.Add(forum);


            }

            IEnumerator<Forum> newForum = Forums.Cast<Forum>().GetEnumerator();

            return View(newForum);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
