﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment_1.Common;
using Assignment_1.Models;
using Microsoft.AspNet.Identity;

namespace Assignment_1.Views
{
    [Authorize(Roles ="Runner")]
    public class BookingController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();



        public ActionResult EventBooking() {

            ApplicationUser user = db.Users.Find(User.Identity.GetUserId());

            return View(db.Bookings.Where(b=> b.ApplicationUser.Id == user.Id).ToList());


        }




        // GET: Booking/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Get the event with the Id
            Event @event = db.Event.Find(id);

            if (@event == null)
            {
                return HttpNotFound();
            }

            if (@event.DateOfEvent < DateTime.Now)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }


            // Get user bookings if available 

            String userId =  User.Identity.GetUserId();

            Bookings bookings = db.Bookings.Where(b => b.Event.EventId == id
            && b.ApplicationUser.Id == userId).FirstOrDefault();

            BookingViewModel bookingViewModel = new BookingViewModel();

            if (bookings == null)
            {

                List<EventTypeSelector> eventTypeSelector = new List<EventTypeSelector>();


                if (@event.FivekmRace)
                {
                    EventTypeSelector eventTypeSelector_ob = new EventTypeSelector();
                    eventTypeSelector_ob.TypeName = "5KM Race";
                    eventTypeSelector_ob.Typevalue = "1";
                    eventTypeSelector.Add(eventTypeSelector_ob);
                }
                if (@event.TenkmRace)
                {
                    EventTypeSelector eventTypeSelector_ob = new EventTypeSelector();
                    eventTypeSelector_ob.TypeName = "10KM Race";
                    eventTypeSelector_ob.Typevalue = "2";
                    eventTypeSelector.Add(eventTypeSelector_ob);
                }
                if (@event.HalfMarathon)
                {
                    EventTypeSelector eventTypeSelector_ob = new EventTypeSelector();
                    eventTypeSelector_ob.TypeName = "Half Matahhon";
                    eventTypeSelector_ob.Typevalue = "3";
                    eventTypeSelector.Add(eventTypeSelector_ob);
                }
                if (@event.FullMarathon)
                {
                    EventTypeSelector eventTypeSelector_ob = new EventTypeSelector();
                    eventTypeSelector_ob.TypeName = "Full Matahhon";
                    eventTypeSelector_ob.Typevalue = "4";
                    eventTypeSelector.Add(eventTypeSelector_ob);
                }


                ViewBag.EventType = new SelectList(eventTypeSelector, "TypeValue", "TypeName");
                bookingViewModel.Bookings = bookings;
                bookingViewModel.Event = @event;

                return View(bookingViewModel);
            }

            else
            {
                
                bookingViewModel.Bookings = bookings;
                bookingViewModel.Event = @event;
                return View(bookingViewModel);
            }

            
        }



        // GET: Booking/Create
        public ActionResult Cancel(int? id)
        {
            String UserId = User.Identity.GetUserId();


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Bookings booking = db.Bookings.Where(m => m.ApplicationUser.Id == UserId && m.Event.EventId == id).FirstOrDefault();


            if (booking == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            

            Event @event = db.Event.Find(booking.Event.EventId);


            if (@event == null)
            {
                return HttpNotFound();
            }

            if (@event.DateOfEvent <= DateTime.Now)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }


            if (!booking.Status)
            {
                return RedirectToAction("Details", new { id = booking.Event.EventId });

            }


            booking.Status = false;
            db.Entry(booking).State = EntityState.Modified;
            db.SaveChanges();


            String toEmail = booking.ApplicationUser.Email;
            String subject = "You Have Cancelled Booking For " + booking.Event.EventName;
            String contents = CreateCancelBody(booking);

            EmailSender es = new EmailSender();
            es.Send(toEmail, subject, contents, "Run 360 Event Cancellation");

            ViewBag.Result = "Email has been send.";




            return RedirectToAction("Details", new { id = booking.Event.EventId } );


        }



        // POST: Booking/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details([Bind(Include = "RaceType, Event")] Bookings bookings, String btn)
        {

            var eventId = int.Parse(btn);

            bookings.Event = db.Event.Find(eventId);

            ModelState.Clear();
            TryValidateModel(bookings);

            if (ModelState.IsValid)
            {
               
               if (bookings.RaceType == "1")
                {
                    bookings.RaceType = "5KM Race";
                }
               else if(bookings.RaceType == "2")
                {
                    bookings.RaceType = "10KM Race";
                }
               else if (bookings.RaceType == "3")
                {
                    bookings.RaceType = "Half marathon";
                }
               else
                {
                    bookings.RaceType = "Full Marathon";
                }

                bookings.ApplicationUser = db.Users.Find(User.Identity.GetUserId());

                bookings.Status = true;

                db.Bookings.Add(bookings);

                db.SaveChanges();


                String toEmail = bookings.ApplicationUser.Email;
                String subject = "You Have Successfully Registered For " + bookings.Event.EventName;
                String contents = CreateBody(bookings);

                EmailSender es = new EmailSender();
                es.Send(toEmail, subject, contents, "Run 360 Event Booking");

                ViewBag.Result = "Email has been send.";


                return RedirectToAction("Details");
            }

            return RedirectToAction("Details");
        }




        private String CreateBody(Bookings booking) {

            String body = String.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Email Templates/index.html")))
            {

                body = reader.ReadToEnd();

            }


            body = body.Replace("{Event}", booking.Event.EventName);
            body = body.Replace("{Id}", booking.Event.EventId.ToString());
            body = body.Replace("{Location}", booking.Event.Location);
            body = body.Replace("{Date}", booking.Event.DateOfEvent.ToShortDateString());
            body = body.Replace("{Event}", booking.Event.EventName);
            body = body.Replace("{bookingId}", booking.Event.EventId.ToString());
            return body;



        }



        private String CreateCancelBody(Bookings booking)
        {

            String body = String.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Email Templates/Cancel.html")))
            {

                body = reader.ReadToEnd();

            }


            body = body.Replace("{Event}", booking.Event.EventName);
            body = body.Replace("{bookingId}", booking.Event.EventId.ToString());
            return body;



        }




        // GET: Booking/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Event.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // POST: Booking/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( [Bind(Include = "EventId,EventName,Location,DateOfEvent,FivekmRace,TenkmRace,HalfMarathon,FullMarathon,City,State,Country,EventType")] Event @event)
        {
            if (ModelState.IsValid)
            {
                db.Entry(@event).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(@event);
        }

        // GET: Booking/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Event.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // POST: Booking/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Event @event = db.Event.Find(id);
            db.Event.Remove(@event);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
