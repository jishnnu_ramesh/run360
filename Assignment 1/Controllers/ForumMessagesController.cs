﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment_1.Common;
using Assignment_1.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace Assignment_1.Controllers
{
    public class ForumMessagesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

      
        // GET: ForumMessages
        public ActionResult Index()
        {
            return View(db.ForumMessages.ToList());
        }



        [HttpPost]
        public void Upvote(int id) {


            ApplicationUser applicationUser = db.Users.Find(User.Identity.GetUserId());
            ForumLike forumLike = new ForumLike();

            if (User.IsInRole("Runner"))
            {
                 forumLike = db.ForumLike.Where(f => f.ApplicationUser.Id == applicationUser.Id && f.ForumMessages.ForumMessageId == id).FirstOrDefault();

            }

            else
            {
                 forumLike = db.ForumLike.Where(f => f.ApplicationUser.Id == applicationUser.Id && f.ForumMessages.ForumMessageId == id).FirstOrDefault();

            }


            ForumMessages forumMessages = db.ForumMessages.Where(o => o.ForumMessageId == id).FirstOrDefault();


            if ( forumMessages != null) {

                if (forumLike == null)
                {
                    ForumLike newForumLike = new ForumLike();

                    newForumLike.ApplicationUser = applicationUser;
                    newForumLike.ForumMessages = forumMessages;
                    newForumLike.Votes = 1;
                    db.ForumLike.Add(newForumLike);
                    db.SaveChanges();


                }

                else { 


                if (forumLike.Votes == 1)
                {
                    forumMessages.Votes = forumMessages.Votes - 1;
                    forumLike.Votes = 0;
                }
                else if (forumLike.Votes == 0)
                {
                    forumMessages.Votes = forumMessages.Votes + 1;
                    forumLike.Votes = 1;

                }
                else
                {
                    forumMessages.Votes = forumMessages.Votes + 1;
                    forumLike.Votes = 0;

                }

                db.Entry(forumLike).State = EntityState.Modified;
                db.Entry(forumMessages).State = EntityState.Modified;
                db.SaveChanges();

                }

            }




        }


        [HttpPost]
        public void DownVote(int id)
        {

            ApplicationUser applicationUser = db.Users.Find(User.Identity.GetUserId());

            ForumLike forumLike = new ForumLike();

            if (User.IsInRole("Runner"))
            {
                forumLike = db.ForumLike.Where(f => f.ApplicationUser.Id == applicationUser.Id && f.ForumMessages.ForumMessageId == id).FirstOrDefault();

            }

            else
            {
                forumLike = db.ForumLike.Where(f => f.ApplicationUser.Id == applicationUser.Id && f.ForumMessages.ForumMessageId == id).FirstOrDefault();

            }



            ForumMessages forumMessages = db.ForumMessages.Where(o => o.ForumMessageId == id).FirstOrDefault();


            if (forumMessages != null)
            {


                if (forumLike == null)
                {
                    ForumLike newForumLike = new ForumLike();

                    newForumLike.ApplicationUser = applicationUser;
                    newForumLike.ForumMessages = forumMessages;
                    newForumLike.Votes = -1;
                    db.ForumLike.Add(newForumLike);
                    db.SaveChanges();


                }

                else
                {




                    if (forumLike.Votes == -1)
                    {
                        forumMessages.Votes = forumMessages.Votes + 1;
                        forumLike.Votes = 0;
                    }
                    else if (forumLike.Votes == 0)
                    {
                        forumMessages.Votes = forumMessages.Votes - 1;
                        forumLike.Votes = -1;

                    }
                    else
                    {
                        forumMessages.Votes = forumMessages.Votes - 1;
                        forumLike.Votes = 0;

                    }

                    db.Entry(forumLike).State = EntityState.Modified;
                    db.Entry(forumMessages).State = EntityState.Modified;
                    db.SaveChanges();


                }
            }


        }



        // GET: ForumMessages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Forum forum = db.Forum.Find(id);

           

            if (forum == null)
            {
                return HttpNotFound();
            }

            Event @event = forum.Event;

            ApplicationUser user = db.Users.Find(User.Identity.GetUserId());


            if (User.IsInRole("Organizer")) {


                if (@event.organizer.Id != user.Id)
                {

                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else {

                

                    IEnumerable<ForumMessages> forumMessages = db.ForumMessages.Where(f => f.Event.EventId == @event.EventId).ToList();
                    IEnumerable<ForumLike> forumLike = db.ForumLike.Where(f => f.ApplicationUser.Id == user.Id).ToList();

                    

                    ForumViewModel forumViewModel = new ForumViewModel();
                    forumViewModel.ForumMessages = forumMessages;
                    forumViewModel.ForumLike = forumLike;
                    forumViewModel.ForumMessagesform = "";
                    forumViewModel.Event = forumMessages.First().Event;

                    try
                    {
                        
                        ViewBag.Message = TempData["shortMessage"].ToString();
                        forumViewModel.ForumMessagesform = TempData["Message"].ToString();
                    }
                    catch
                    {
                        Console.WriteLine("No Issues");
                    }

                   
                    return View(forumViewModel);
                }

            }

            else {

                Bookings bookings = db.Bookings.Where(b => b.ApplicationUser.Id == user.Id && b.Event.EventId == @event.EventId).FirstOrDefault();

                if (bookings == null)
                {

                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);


                }

                else {


                    IEnumerable<ForumMessages> forumMessages = db.ForumMessages.Where(f => f.Event.EventId == @event.EventId).ToList() ;
                    IEnumerable<ForumLike> forumLike = db.ForumLike.Where(f => f.ApplicationUser.Id == user.Id).ToList();

                    ForumViewModel forumViewModel = new ForumViewModel();
                    forumViewModel.ForumMessages = forumMessages;
                    forumViewModel.ForumMessagesform = "";
                    forumViewModel.ForumLike = forumLike;
                    forumViewModel.Event = forumMessages.First().Event;
                    try
                    {
                        ViewBag.Message = TempData["shortMessage"].ToString();
                        forumViewModel.ForumMessagesform = TempData["Message"].ToString();
                    }
                    catch
                    {
                        Console.WriteLine("No Issues");
                    }

                    return View(forumViewModel);


                    // return View(db.ForumMessages.Where(f => f.Event.EventId == @event.EventId).ToList());



                }



            }


        }



        /* Code used from the following source
        - Gnanavel Sekar
        - 11/12/2017
        - Integrate Google ReCaptcha And Validate It With ASP.NET MVC 5 And Entity Framework
        - source code
        - https://www.c-sharpcorner.com/article/integrate-google-recaptcha-and-validate-it-with-asp-net-mvc5-and-entity-framewor/
        */

        public static CaptchaResponse ValidateCaptcha(string response)
        {
            var secret = "6LflgnQUAAAAAKIKjgfJ0gOapj7SeLJKTja3 - e0G";
            var client = new WebClient();
            var jsonResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));
            return JsonConvert.DeserializeObject<CaptchaResponse>(jsonResult.ToString());
        }




        // GET: ForumMessages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ForumMessages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Details([Bind(Include = "ForumMessagesform")] String forumMessagesform, String btn, int ? id, HttpPostedFileBase postedFile)
        {


            /* Code used from the following source
                - Gnanavel Sekar
               -  11/12/2017
               -  Integrate Google ReCaptcha And Validate It With ASP.NET MVC 5 And Entity Framework
               - source code
               - https://www.c-sharpcorner.com/article/integrate-google-recaptcha-and-validate-it-with-asp-net-mvc5-and-entity-framewor/
            */

            CaptchaResponse response = ValidateCaptcha(Request["g-recaptcha-response"]);


            if (!response.Success)
            {

               
                TempData["shortMessage"] = "Your Forum Post was not saved due to invalid captcha";
                TempData["Message"] = forumMessagesform;
                return RedirectToAction("Details", new { @id = id });

            }

            int EventId;

            try
            {

                EventId = int.Parse(btn);
               
            }
            catch (Exception Ex)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            

            if (forumMessagesform == null ||  forumMessagesform.Length == 0 )
            {

                ModelState.AddModelError("forumMessagesform", "You need to enter forum message");
                TempData["shortMessage"] = "Your Forum Post was not saved as it was empty";


            }


            // object to save
            ForumMessages forumMessages = new ForumMessages();


            if (postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                if (Util.IsRecognisedImageFile(postedFile.FileName))
                {
                    // Here I want each image to have a unique path. Doesn't matter if the same image is uploaded. E
                    // Each path is the same length.
                    String filePath = Util.GenerateUniqueString();
                    filePath = Util.CalculateMD5Hash(filePath);

                    String fileExtension = Path.GetExtension(postedFile.FileName);
                    String full = filePath + fileExtension;
                    postedFile.SaveAs(path + full);
                    forumMessages.Image = full;
                }
                else
                {
                    ModelState.AddModelError("forumMessagesform", "You need to enter forum message");
                    TempData["shortMessage"] = "Please enter a valid image ";
                    return RedirectToAction("Details", new { @id = id });

                }
            }

            else {


                forumMessages.Image = null;

            }




            if (ModelState.IsValid)
            {
                
                forumMessages.ApplicationUser = db.Users.Find(User.Identity.GetUserId());
                forumMessages.Event = db.Event.Find(EventId);
                forumMessages.Date = DateTime.Now;
                forumMessages.Active = true;
                forumMessages.Votes = 0;
                forumMessages.Message = forumMessagesform;
                db.ForumMessages.Add(forumMessages);
                db.SaveChanges();

                ForumLike forumLike = new ForumLike();
                forumLike.ApplicationUser = db.Users.Find(User.Identity.GetUserId());
                forumLike.ForumMessages = forumMessages;
                forumLike.Votes = 0;
                db.ForumLike.Add(forumLike);
                db.SaveChanges();

                return RedirectToAction("Details",new { @id = id });
            }

            return RedirectToAction("Details", new { @id = id });
        }






        // GET: ForumMessages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ForumMessages forumMessages = db.ForumMessages.Find(id);

            if (forumMessages == null)
            {
                return HttpNotFound();
            }

            ApplicationUser user = db.Users.Find(User.Identity.GetUserId());




            if (forumMessages.ApplicationUser.Id != user.Id) {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }


            if (!forumMessages.Active)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }


            ForumEditViewModel forumEditViewModel = new ForumEditViewModel();
            forumEditViewModel.Message = forumMessages.Message;
            forumEditViewModel.Id = forumMessages.ForumMessageId;

            return View(forumEditViewModel);

        }

        // POST: ForumMessages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Message")] ForumEditViewModel forumEditViewModel, HttpPostedFileBase postedFile)
        {


            CaptchaResponse response = ValidateCaptcha(Request["g-recaptcha-response"]);


            if (!response.Success)
            {


                ViewBag.Error =  "Your Forum Post was not saved due to invalid captcha";
                
                return View(forumEditViewModel);

            }


            ForumMessages fm = db.ForumMessages.Find(forumEditViewModel.Id);
           

            if (fm == null)
            {

                ViewBag.Error = "Server Error";

                return View(forumEditViewModel);



            }

            Forum forum = db.Forum.Where(f => f.Event.EventId == fm.Event.EventId).FirstOrDefault();




            /* Code used from the following source
            - Jian Liew
            - 29/7/2018
            - FILE UPLOAD   
            - source code
            - https://moodle.vle.monash.edu/pluginfile.php/7453110/mod_resource/content/36/FIT5032-W08B-File%20Upload%281%29.pdf
            */


            if (postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                if (Util.IsRecognisedImageFile(postedFile.FileName))
                {
                    
                    String filePath = Util.GenerateUniqueString();
                    filePath = Util.CalculateMD5Hash(filePath);

                    String fileExtension = Path.GetExtension(postedFile.FileName);
                    String full = filePath + fileExtension;
                    postedFile.SaveAs(path + full);
                    fm.Image = full;
                }
                else
                {
                    ModelState.AddModelError("forumMessagesform", "You need to enter forum message");
                    TempData["shortMessage"] = "Please enter a valid image ";
                    return RedirectToAction("Details", new { @id = forum.ForumId });

                }
            }



            if (ModelState.IsValid)
            {
                fm.Message = forumEditViewModel.Message;
                db.Entry(fm).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", new { @id = forum.ForumId });
            }
            return View(forumEditViewModel);
        }




        // GET: ForumMessages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ForumMessages forumMessages = db.ForumMessages.Find(id);
            Forum forum = db.Forum.Where(f => f.Event.EventId == forumMessages.Event.EventId).FirstOrDefault();
            int Id = forum.ForumId;

            if (forumMessages == null)
            {
                return HttpNotFound();
            }

            ApplicationUser user = db.Users.Find(User.Identity.GetUserId());

            if (forumMessages.ApplicationUser.Id != user.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }


            db.ForumMessages.Remove(forumMessages);
            db.SaveChanges();
            return RedirectToAction("Details", new { @id = Id });

        }





        // POST: ForumMessages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ForumMessages forumMessages = db.ForumMessages.Find(id);
            int Id = forumMessages.ForumMessageId;

            if (forumMessages == null)
            {
                return HttpNotFound();
            }

            ApplicationUser user = db.Users.Find(User.Identity.GetUserId());

            if (forumMessages.ApplicationUser.Id != user.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }


            db.ForumMessages.Remove(forumMessages);
            db.SaveChanges();
            return RedirectToAction("Details", new { @id = Id});
        }




        // Disapprove post
        public ActionResult Disapprove(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ForumMessages forumMessages = db.ForumMessages.Find(id);
            Forum forum = db.Forum.Where(f => f.Event.EventId == forumMessages.Event.EventId).FirstOrDefault();
            int Id = forum.ForumId;

            if (forumMessages == null)
            {
                return HttpNotFound();
            }

            ApplicationUser user = db.Users.Find(User.Identity.GetUserId());

            if (User.IsInRole("Organizer"))
            {

                forumMessages.Active = false;
                db.Entry(forumMessages).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", new { @id = Id });

            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }



        }






        [Authorize(Roles ="Organizer")]
        public ActionResult SendEmail( int ? id) {


            if (id == null) {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }


            Event @event = db.Event.Where(f => f.EventId == id).FirstOrDefault();


            if (@event.organizer.Id != User.Identity.GetUserId()) {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }


            ForumViewModel fm = new ForumViewModel();
            fm.Event = @event;

            return View(fm);

        }



        [Authorize(Roles = "Organizer")]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult SendEmail([Bind(Include = "ForumMessagesform")] String forumMessagesform, String btn)
        {

            var Id = 0;

            try
            {

                Id = int.Parse(btn);
            }
            catch (Exception ex)
            {

                Id = 0;
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);


            }



            if (Id == 0)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }


            Event @event = db.Event.Where(f => f.EventId == Id).FirstOrDefault();


            if (@event.organizer.Id != User.Identity.GetUserId())
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }


            Forum forum = db.Forum.Where(f => f.Event.EventId == Id).FirstOrDefault();

            if (forum == null)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }

            CaptchaResponse response = ValidateCaptcha(Request["g-recaptcha-response"]);


            if (!response.Success)
            {

                

                TempData["shortMessage"] = "Your Email was not send due to  invalid captcha";
                TempData["Message"] = "";
                return RedirectToAction("Details", new { @id = forum.ForumId });

            }

            IEnumerable<Bookings> bookings = db.Bookings.Where(b => b.Event.EventId == Id).ToList();
            String subject = @event.EventName + " Forum Mail";
            String contents = CreateEmailBody(forumMessagesform, @event);

            EmailSender es = new EmailSender();
            es.SendMultiple(bookings, subject, contents, @event.EventName + " Forum Mail ");


            TempData["shortMessage"] = "Your Emails was sent";
            TempData["Message"] = forumMessagesform;
            return RedirectToAction("Details", new { @id = forum.ForumId });

        }





        // https://www.youtube.com/watch?v=FFg4lK6KHz4

        private string CreateEmailBody(string forumMessagesform, Event @event)
        {


            String body = String.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Email Templates/PersonalMail.html")))
            {

                body = reader.ReadToEnd();

            }


            body = body.Replace("{Event}", @event.EventName);
            body = body.Replace("{message}", forumMessagesform);
            return body;




        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
