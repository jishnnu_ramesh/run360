﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment_1.Models;
using Microsoft.AspNet.Identity;

namespace Assignment_1.Controllers
{
    [Authorize (Roles ="Runner")]
    public class RunnerProfilesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: RunnerProfiles/Index/
        public ActionResult Index()
        {
            string Id = User.Identity.GetUserId();

            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            RunnerProfile runnerProfile = db.RunnerProfile.Where(o => o.ApplicationUser.Id == Id).FirstOrDefault();

            if (runnerProfile == null)
            {
                return RedirectToAction("Create");
            }

            return View(runnerProfile);
        }



        // GET: RunnerProfiles/Create
        public ActionResult Create()
        {
            string Id = User.Identity.GetUserId();
            RunnerProfile runnerProfile = db.RunnerProfile.Where(o => o.ApplicationUser.Id == Id).FirstOrDefault();

            if (runnerProfile == null)
            {

                return View();
            }
            else
            {
                return RedirectToAction("Edit");
            }

        }



        // POST: RunnerProfiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProfileId,FirstName,LastName,DateOfBirth,Height,weight")] RunnerProfile runnerProfile)
        {
           if (ModelState.IsValid)
            {
                runnerProfile.ApplicationUser = db.Users.Find(User.Identity.GetUserId());
                db.RunnerProfile.Add(runnerProfile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(runnerProfile);
        }



        // GET: RunnerProfiles/Edit
        public ActionResult Edit()
        {
            string Id = User.Identity.GetUserId();
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            RunnerProfile runnerProfile = db.RunnerProfile.Where(o => o.ApplicationUser.Id == Id).FirstOrDefault();

            if (runnerProfile == null)
            {
                return RedirectToAction("Create");
            }

            return View(runnerProfile);
        }



        // POST: RunnerProfiles/Edit
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProfileId,FirstName,LastName,DateOfBirth,Height,weight")] RunnerProfile runnerProfile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(runnerProfile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(runnerProfile);
        }



        // Delete and delete confirmed needs to be implemented with views

        // GET: RunnerProfiles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RunnerProfile runnerProfile = db.RunnerProfile.Find(id);
            if (runnerProfile == null)
            {
                return HttpNotFound();
            }
            return View(runnerProfile);
        }

        // POST: RunnerProfiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RunnerProfile runnerProfile = db.RunnerProfile.Find(id);
            db.RunnerProfile.Remove(runnerProfile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
