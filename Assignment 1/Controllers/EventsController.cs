﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Assignment_1.Models;
using Geocoding;
using Geocoding.Google;
using Microsoft.AspNet.Identity;

namespace Assignment_1.Controllers
{
    [Authorize (Roles ="Organizer")]
    public class EventsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private static readonly String firstForumMessage = "<ol class=\"c3 lst-kix_d969wq2pqpqh-0 start\" style=\"margin: 0px; padding: 0px; list-style-type: none; counter-reset: lst-ctn-kix_d969wq2pqpqh-0 0; color: #545454; font-family: Helvetica, arial, sans-serif; font-size: 16px;\" start=\"1\"><li class=\"c0\" style=\"margin: 15px 0px 15px 36pt; padding: 0px 0px 0px 0pt; font-family: Arial; -webkit-font-smoothing: antialiased; color: #000000; font-size: 11pt; direction: ltr; counter-increment: lst-ctn-kix_d969wq2pqpqh-0 1;\"><strong><span style=\"margin: 0px; padding: 0px;\">FORUM RULES AND GUIDELINES</span></strong></li><li class=\"c0\" style=\"margin: 15px 0px 15px 36pt; padding: 0px 0px 0px 0pt; font-family: Arial; -webkit-font-smoothing: antialiased; color: #000000; font-size: 11pt; direction: ltr; counter-increment: lst-ctn-kix_d969wq2pqpqh-0 1;\"><span style=\"margin: 0px; padding: 0px;\">1. No spam. All automated messages, advertisements, and links to competitor websites will be deleted immediately.</span></li></ol><ol class=\"c3 lst-kix_d969wq2pqpqh-0\" style=\"margin: 0px; padding: 0px; list-style-type: none; color: #545454; font-family: Helvetica, arial, sans-serif; font-size: 16px;\" start=\"2\"><li class=\"c0\" style=\"margin: 15px 0px 15px 36pt; padding: 0px 0px 0px 0pt; font-family: Arial; -webkit-font-smoothing: antialiased; color: #000000; font-size: 11pt; direction: ltr; counter-increment: lst-ctn-kix_d969wq2pqpqh-0 1;\"><span style=\"margin: 0px; padding: 0px;\">2. Messages posted in the wrong topic area will be removed&nbsp;</span></li></ol><ol class=\"c3 lst-kix_d969wq2pqpqh-0\" style=\"margin: 0px; padding: 0px; list-style-type: none; color: #545454; font-family: Helvetica, arial, sans-serif; font-size: 16px;\" start=\"3\"><li class=\"c0\" style=\"margin: 15px 0px 15px 36pt; padding: 0px 0px 0px 0pt; font-family: Arial; -webkit-font-smoothing: antialiased; color: #000000; font-size: 11pt; direction: ltr; counter-increment: lst-ctn-kix_d969wq2pqpqh-0 1;\"><span style=\"margin: 0px; padding: 0px;\">3. Respect other users. No flaming or abusing fellow forum members. Users who continue to post inflammatory, abusive comments will be deleted from the forum&nbsp;</span></li></ol><ol class=\"c3 lst-kix_d969wq2pqpqh-0\" style=\"margin: 0px; padding: 0px; list-style-type: none; color: #545454; font-family: Helvetica, arial, sans-serif; font-size: 16px;\" start=\"4\"><li class=\"c0\" style=\"margin: 15px 0px 15px 36pt; padding: 0px 0px 0px 0pt; font-family: Arial; -webkit-font-smoothing: antialiased; color: #000000; font-size: 11pt; direction: ltr; counter-increment: lst-ctn-kix_d969wq2pqpqh-0 1;\"><span style=\"margin: 0px; padding: 0px;\">4. Harassment. No threats or harassment of other users will be tolerated. Any instance of threatening or harassing behavior is grounds for deletion from the forums.</span></li></ol><ol class=\"c3 lst-kix_d969wq2pqpqh-0\" style=\"margin: 0px; padding: 0px; list-style-type: none; color: #545454; font-family: Helvetica, arial, sans-serif; font-size: 16px;\" start=\"5\"><li class=\"c0\" style=\"margin: 15px 0px 15px 36pt; padding: 0px 0px 0px 0pt; font-family: Arial; -webkit-font-smoothing: antialiased; color: #000000; font-size: 11pt; direction: ltr; counter-increment: lst-ctn-kix_d969wq2pqpqh-0 1;\"><span style=\"margin: 0px; padding: 0px;\">5. Adult content. No profanity or pornography is allowed. Posts containing adult material will be deleted.</span></li></ol><ol class=\"c3 lst-kix_d969wq2pqpqh-0\" style=\"margin: 0px; padding: 0px; list-style-type: none; color: #545454; font-family: Helvetica, arial, sans-serif; font-size: 16px;\" start=\"6\"><li class=\"c0\" style=\"margin: 15px 0px 15px 36pt; padding: 0px 0px 0px 0pt; font-family: Arial; -webkit-font-smoothing: antialiased; color: #000000; font-size: 11pt; direction: ltr; counter-increment: lst-ctn-kix_d969wq2pqpqh-0 1;\"><span style=\"margin: 0px; padding: 0px;\">6. Bandwidth. All images and signatures must be 500 x 500 pixels or smaller. Posts containing over-sized images and signatures will be removed.</span></li></ol><ol class=\"c3 lst-kix_d969wq2pqpqh-0\" style=\"margin: 0px; padding: 0px; list-style-type: none; color: #545454; font-family: Helvetica, arial, sans-serif; font-size: 16px;\" start=\"7\"><li class=\"c0\" style=\"margin: 15px 0px 15px 36pt; padding: 0px 0px 0px 0pt; font-family: Arial; -webkit-font-smoothing: antialiased; color: #000000; font-size: 11pt; direction: ltr; counter-increment: lst-ctn-kix_d969wq2pqpqh-0 1;\"><span style=\"margin: 0px; padding: 0px;\">7. Illegal content. No re-posting of copyrighted materials or other illegal content is allowed. Any posts containing illegal content or copyrighted materials will be deleted.</span></li></ol>";

        private String WELCOME_MESSAGE = " Welcome to the forum for ";

        // GET: Events
        public ActionResult Index()
        {
            String Id = User.Identity.GetUserId();
            return View(db.Event.Where(o => o.organizer.Id == Id).ToList());
        }

        // GET: Events/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            String UserId = User.Identity.GetUserId();

            Event @event = db.Event.Where(o => o.organizer.Id == UserId &&
          o.EventId == id).FirstOrDefault();

            if (@event == null)
            {
                return HttpNotFound();
            }

            return View(@event);
        }

        // GET: Events/Create
        public ActionResult Create()
        {
            ViewBag.EventType = new SelectList(db.EventType, "EventId", "TypeOfEvent");
            return View();
        }

        // POST: Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create ([Bind(Include = "EventId,EventName,Location,DateOfEvent,FivekmRace,TenkmRace,HalfMarathon,FullMarathon,EventType")] Event @event)
        {
            try
            {
                GoogleGeocoder geocoder = new GoogleGeocoder() { ApiKey = "AIzaSyA6rc80PyXbcRDZwgm8RBfoSLmRI6nNEEg" };
                IEnumerable<GoogleAddress> addresses = await geocoder.GeocodeAsync(@event.Location);

                if (addresses.First() == null || addresses.First().Equals(""))
                {

                    ModelState.AddModelError("Location", "The location you entered is not valid");
                }

                if (ModelState.IsValid)
                {
                    @event.Location = addresses.First().FormattedAddress;

                    try
                    {
                        var Country = addresses.Where(a => !a.IsPartialMatch).Select(a => a[GoogleAddressType.Country]).First();
                        @event.Country = Country.LongName;
                        
                    }
                    finally {

                        @event.organizer = db.Users.Find(User.Identity.GetUserId());
                        db.Event.Add(@event);
                        db.SaveChanges();


                    }
                   
                    // Create Forum

                    Forum forum = new Forum();
                    forum.Date = DateTime.Now;
                    forum.Event = @event;
                    forum.ApplicationUser = db.Users.Find(User.Identity.GetUserId());
                    forum.WelcomeMessage = WELCOME_MESSAGE + @event.EventName;
                    db.Forum.Add(forum);
                    db.SaveChanges();


                    ForumMessages forumMessages = new ForumMessages();
                    forumMessages.Active = true;
                    forumMessages.Date = DateTime.Now;
                    forumMessages.ApplicationUser = db.Users.Find(User.Identity.GetUserId());
                    forumMessages.Event = @event;
                    forumMessages.ForumMessageId = forum.ForumId;
                    forumMessages.Votes = 0;
                    forumMessages.Message = firstForumMessage;
                    db.ForumMessages.Add(forumMessages);
                    db.SaveChanges();



                    return RedirectToAction("Index");
                }

            }
            catch
            {
                ModelState.AddModelError("Location", "The location you entered is not valid");

            }   

            ViewBag.EventType = new SelectList(db.EventType, "EventId", "TypeOfEvent");
            return View(@event);
        }


        // GET: Events/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            String UserId = User.Identity.GetUserId();

            Event @event = db.Event.Where(o => o.organizer.Id == UserId &&
          o.EventId == id).FirstOrDefault();

            if (@event == null)
            {
                return HttpNotFound();
            }

            if (@event.DateOfEvent <= DateTime.Now)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }

            ViewBag.EventType = new SelectList(db.EventType, "EventId", "TypeOfEvent");
            return View(@event);
        }



        // POST: Events/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "EventId,EventName,Location,DateOfEvent,FivekmRace,TenkmRace,HalfMarathon,FullMarathon,EventType")] Event @event)
        {

            try
            {
                IGeocoder geocoder = new GoogleGeocoder() { ApiKey = "AIzaSyA6rc80PyXbcRDZwgm8RBfoSLmRI6nNEEg" };
                IEnumerable<Address> addresses = await geocoder.GeocodeAsync(@event.Location);

                if (addresses.First() == null || addresses.First().Equals(""))
                {

                    ModelState.AddModelError("Location", "The location you entered is not valid");
                }

                if (ModelState.IsValid)
                {
                    @event.Location = addresses.First().FormattedAddress;
                    db.Entry(@event).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                ModelState.AddModelError("Location", "The location you entered is not valid");

            }

            
            return View(@event);
        }


        // GET: Events/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            String UserId = User.Identity.GetUserId();

            Event @event = db.Event.Where(o => o.organizer.Id == UserId &&
          o.EventId == id).FirstOrDefault();

            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }


        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            String UserId = User.Identity.GetUserId();

            Event @event = db.Event.Where(o => o.organizer.Id == UserId &&
          o.EventId == id).FirstOrDefault();

            if (@event == null)
            {
                return HttpNotFound();
            }

            else
            {
                db.Event.Remove(@event);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
