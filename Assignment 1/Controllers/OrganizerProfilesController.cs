﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment_1.Models;
using Microsoft.AspNet.Identity;

namespace Assignment_1.Controllers
{
    [Authorize(Roles = "Organizer")]
    public class OrganizerProfilesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: OrgProfiles/Details/
        [Authorize(Roles = "Organizer")]
        public ActionResult Index()
        {

            string Id = User.Identity.GetUserId();

            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            OrganizerProfile organizerProfile = db.OrganizerProfile.Where(o => o.ApplicationUser.Id == Id).FirstOrDefault();

            if (organizerProfile == null)
            {
                return RedirectToAction("Create");
            }
            return View(organizerProfile);
        }


        // GET: OrganizerProfiles/Create
        [Authorize(Roles ="Organizer")]
        public ActionResult Create()
        {
            string Id = User.Identity.GetUserId();
            OrganizerProfile organizerProfile = db.OrganizerProfile.Where(o => o.ApplicationUser.Id == Id).FirstOrDefault();
            if (organizerProfile == null) {
                return View();
            }
            else
            {
                return RedirectToAction("Edit");
            }
             
        }


        // POST: OrganizerProfiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Organizer")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProfileId,FirstName,LastName,DateOfBirth")] OrganizerProfile organizerProfile)
        {
            if (ModelState.IsValid)
            {
                organizerProfile.ApplicationUser = db.Users.Find(User.Identity.GetUserId());
                db.OrganizerProfile.Add(organizerProfile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(organizerProfile);
        }


        // GET: OrganizerProfiles/Edit/
        [Authorize (Roles ="Organizer")]
        public ActionResult Edit()
        {
            string Id = User.Identity.GetUserId();
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizerProfile organizerProfile = db.OrganizerProfile.Where(o => o.ApplicationUser.Id == Id).FirstOrDefault();
            if (organizerProfile == null)
            {
                return RedirectToAction("Create");
            }
            return View(organizerProfile);
        }


        // POST: OrganizerProfiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Organizer")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProfileId,FirstName,LastName,DateOfBirth")] OrganizerProfile organizerProfile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organizerProfile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(organizerProfile);
        }




        // Delete and delete confirmed needs to be implemented properly with views

        // GET: OrganizerProfiles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganizerProfile organizerProfile = db.OrganizerProfile.Find(id);
            if (organizerProfile == null)
            {
                return HttpNotFound();
            }
            return View(organizerProfile);
        }

        // POST: OrganizerProfiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrganizerProfile organizerProfile = db.OrganizerProfile.Find(id);
            db.OrganizerProfile.Remove(organizerProfile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
