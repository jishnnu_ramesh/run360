﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Assignment_1.Common
{
    
        public class TodayOrFutureDateValidator : ValidationAttribute
        {
            DateTime _today;

            public TodayOrFutureDateValidator()
            {
                _today = DateTime.Now;
            }

            public override bool IsValid(object value)
            {
                DateTime date;

                if (value != null)
                {

                    if (DateTime.TryParse(value.ToString(), out date))
                    {
                        return date >= _today;
                    }

                }

                return false;
            }
        }
}