﻿using Assignment_1.Models;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/* Code used from the following source
- Jian Liew
- 29/7/2018
- SENDING EMAIL (USING SENDGRID)    
- source code
- https://moodle.vle.monash.edu/pluginfile.php/7364484/mod_resource/content/35/FIT5032-W08A-Sending%20Email%20using%20SendGrid.pdf
*/


namespace Assignment_1.Common
{
    public class EmailSender
    {

        // Please use your API KEY here.
        private const String API_KEY = "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   ";

        public void Send(String toEmail, String subject, String contents, String Name)
        {
            var client = new SendGridClient(API_KEY);
            var from = new EmailAddress("noreply@localhost.com", Name);
            var to = new EmailAddress(toEmail);
            var plainTextContent = contents;
            var htmlContent = "<p>" + contents + "</p>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = client.SendEmailAsync(msg);
        }

        public void SendMultiple(IEnumerable<Bookings> bookings, string subject, string contents, string Name)
        {

            foreach (Bookings b in bookings) {


                var client = new SendGridClient(API_KEY);
                var from = new EmailAddress("noreply@localhost.com", Name);
                var to = new EmailAddress(b.ApplicationUser.Email, "jish_r@live.com");
                var plainTextContent = contents;
                var htmlContent = "<p>" + contents + "</p>";
                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                var response = client.SendEmailAsync(msg);



            }

            

        }






    }
}