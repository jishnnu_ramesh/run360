﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Assignment_1.Common
{
    public class IsTrueValidator : ValidationAttribute
    {
        bool _isValid;

        public IsTrueValidator()
        {

        }

        public override bool IsValid(object value)
        {
            if (value != null)
            {
                if (Boolean.TryParse(value.ToString(), out _isValid))

                    return _isValid == true;
            }

            return false;

        }
    }
}