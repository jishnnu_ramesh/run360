﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Assignment_1.Common
{
    public class AgeAttribute:ValidationAttribute
    {
        
        int _minimumAge;

        public AgeAttribute(int minimumAge)
        {
            _minimumAge = minimumAge;
        }

        public override bool IsValid(object value)
        {
            DateTime date;

            if (value != null) {

                if (DateTime.TryParse(value.ToString(), out date))
                {
                    return date.AddYears(_minimumAge) < DateTime.Now;
                }

            }

            return false;
        }

       



    }
}