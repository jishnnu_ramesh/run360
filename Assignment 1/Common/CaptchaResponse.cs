﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/* Code used from the following source
- Gnanavel Sekar
- 11/12/2017
- 	
Integrate Google ReCaptcha And Validate It With ASP.NET MVC 5 And Entity Framework
- source code
- https://www.c-sharpcorner.com/article/integrate-google-recaptcha-and-validate-it-with-asp-net-mvc5-and-entity-framewor/
*/

namespace Assignment_1.Common
{
    public class CaptchaResponse
    {
        [JsonProperty("success")]
        public bool Success
        {
            get;
            set;
        }
        [JsonProperty("error-codes")]
        public List<string> ErrorMessage
        {
            get;
            set;
        }
    }
}