﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1.Models
{
    public class HomeModel
    {
        public SearchViewModel SearchViewModel { get; set; }
        public IEnumerable<Event> Event { get; set; }
        public HashSet<Event> SearchResult { get; set; }
        public bool ResultAvailable { get; set; }



    }
}