﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Assignment_1.Models
{
    public class Bookings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BookingId { get; set; }


        [Required]
        public String RaceType { get; set; }

        [Required]
        public bool Status { get; set; }


        public virtual ApplicationUser ApplicationUser { get; set; }

        [Required]
        public virtual Event Event { get; set; }

       
    }
}