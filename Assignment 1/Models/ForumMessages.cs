﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Models
{
    public class ForumMessages
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ForumMessageId { get; set; }


        [Required]
        public virtual Event Event { get; set; }


        [Required]
        public virtual ApplicationUser ApplicationUser { get; set; }


        [Required]
        [AllowHtml]
        public String Message { get; set; }

        [Required]
        public int Votes { get; set; }

        [Required]
        public Boolean Active { get; set; }

        [Required]
        public DateTime Date { get; set; }


        public String Image { get; set; }

       
    }
}