﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Assignment_1.Models
{
    public class Forum
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ForumId { get; set; }


        [Required]
        public virtual Event Event { get; set; }


        [Required]
        public virtual ApplicationUser ApplicationUser { get; set; }


        [Required]
        public String WelcomeMessage { get; set; }

        [Required]
        public DateTime Date { get; set; }


    }
}