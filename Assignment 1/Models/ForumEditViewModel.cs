﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Models
{
    public class ForumEditViewModel
    {

        [Required]
        public int Id { get; set; }


        [AllowHtml]
        [Required]
        public String Message { get; set; }
    }
}