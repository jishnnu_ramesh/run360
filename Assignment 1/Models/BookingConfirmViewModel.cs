﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Assignment_1.Models
{
    public class BookingConfirmViewModel
    {
        [Required]
        public String RaceType { get; set; }
    }
}