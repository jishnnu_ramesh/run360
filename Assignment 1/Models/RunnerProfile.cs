﻿using Assignment_1.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Assignment_1.Models
{
    public class RunnerProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProfileId { get; set; }

        [Required]
        [RegularExpression("[A-Za-z ]+", ErrorMessage = "Your first name should contain only alphabets")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression("[A-Za-z ]+", ErrorMessage = "Your  name should contain only alphabets")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Date of Birth")]
        [AgeAttribute(18, ErrorMessage = "You must be 18 years old to use this app")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Column(TypeName = "datetime")]
        public DateTime DateOfBirth { get; set; }


        [Required]
        [Display(Name = "Height (in cms)")]
        [Range(30,240, ErrorMessage = "You must enter a valid height")]
        public double Height { get; set; }


        [Required]
        [Display(Name = "Weight (in cms)")]
        [Range(30, 400, ErrorMessage = "You must enter a valid weight" )]
        public double weight { get; set; }



        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}