﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Assignment_1.Common;

namespace Assignment_1.Models
{
    public class SearchViewModel
    {
       
        [Required(ErrorMessage ="Please enter the location or event name to search ")]
        public String SearchText { get; set; }


        [Required(ErrorMessage ="Date Required")]
        [TodayOrFutureDateValidator(ErrorMessage = " You have entered invalid date")]
        public DateTime? StartDate { get; set; }

        [Required(ErrorMessage = "Date Required")]
        [TodayOrFutureDateValidator(ErrorMessage = " You have entered invalid date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? EndDate { get; set; }


    }
}