﻿using Assignment_1.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Models
{
    public class Event
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventId { get; set; }

        [Required]
        [Display(Name = "Event Name")]
        [RegularExpression("[A-Z0-9][A-Za-z0-9 ]+", ErrorMessage = "Event Name should starts with capital letters or numbers. Rest should be alpha numeric characters")]
        public string EventName { get; set; }

        [Required]
        [Display(Name = "Location of Event")]
        public string Location { get; set; }


        [Required]
        [Display(Name = "Event Date")]
        [DateValidator(ErrorMessage = "Date in Future")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Column(TypeName = "datetime")]
        public DateTime DateOfEvent { get; set; }

        [Required]
        [Display(Name = "5 Km Race")]
        [Column(TypeName = "bit")]
        public bool FivekmRace { get; set; } 

        [Required]
        [Display(Name = "10 Km Race")]
        [Column(TypeName = "bit")]
        public bool TenkmRace { get; set; }

        [Required]
        [Display(Name = "Half Marathon")]
        [Column(TypeName = "bit")]
        public bool HalfMarathon { get; set; }

        [Required]
        [Display(Name = "Full Marathon")]
        [Column(TypeName = "bit")]
        public bool FullMarathon { get; set; }


        public string City { get; set; }

        public string State { get; set; }

        public string Country { get; set; }



        [Required]
        [Display(Name = "Event Type")]
        [Column(TypeName ="varchar")]
        public string EventType { get; set; }


        public virtual ApplicationUser organizer { get; set; }


    }
}