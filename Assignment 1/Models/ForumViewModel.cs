﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Models
{
    public class ForumViewModel
    {

        public IEnumerable<ForumMessages> ForumMessages { get; set; }
        public IEnumerable<ForumLike> ForumLike { get; set; }
        [AllowHtml]
        [Required]
        public String ForumMessagesform { get; set; }
        public Event Event { get; set; }
        
    }
}