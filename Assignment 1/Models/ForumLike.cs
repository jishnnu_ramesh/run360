﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Assignment_1.Models
{
    public class ForumLike
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ForumLikeId { get; set; }


        [Required]
        public virtual ForumMessages ForumMessages { get; set; }


        public virtual ApplicationUser ApplicationUser { get; set; }

        [Required]
        public int Votes { get; set; }


    }
}